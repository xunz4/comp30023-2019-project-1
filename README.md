# COMP30023 2019 Project 1
Please check the Notice(important)

## Reference
Some of code and techniques come from: 
"http-server.c" from Lab 6
"server-1.2.c" from Lab 5

All of the reference documents can be find in ./doc folder

## Running
Run Makefile (```$ make```), and then `image_tagger.exe` will be created at root folder.
And then run ```$ ./image_tagger <ip_addr> <port>``` to start the server program

## NOTICE[IMPORTANT]
1. This program use windows html files to test, and all the html files is inside ./views folder.
If the html files is in the root folder, please change the file_dir const at line 40-46 in `server.c` file and make again !!!!!!!

2. Since some of the lib in the program is only available on linux system, the frequently commit is inevitably. In fact, I commit my code for even small change in order to pull my code down into VM. There might be severval commits just for fixing a single bug.

3. This program didn't realize Advanced Functionality 2: Identifying Players by Cookie.

4. This student suffers from OCD, in her opinion, test branch is not clean in the whole project. So she deleted all test branch after merged them to master branch. If you didn't see any branch except master branch, it does not mean that no other branches were created during the work.

## Structure
```
comp30023-2019-project-1
├─┬ doc                     store all the reference files
│ ├── http-server.c
│ ├── image_tagger.pdf
│ └── server-1.2.c
├─┬ views                   store all the html files
│ ├── 1_intro.html
│ ├── 2_start.html
│ ├── 3_first_turn.html
│ ├── 4_accepted.html
│ ├── 5_discarded.html
│ ├── 6_endgame.html
│ └── 7_gameover.html
├── .gitignore 
├── Makefile
├── README.md               Contains info about the project 
├── image_tagger.exe        program made in the situation of working PC
└── server.c                project source code
```

## Acknowledgement
This project completed by Xun Zhang (854776)